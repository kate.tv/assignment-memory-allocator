#include "test.h"

struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}


bool test_1() {
    printf("Starting Test 1...\n");
    bool test_result = true;
    struct block_header* block_1 = block_get_header(_malloc(1024));
    if (!block_1) {
        printf("Failed to create block!\n");
        test_result = false;
    }


    debug_heap(stdout, block_1);

    if (block_1->capacity.bytes < 1024 || block_1->is_free) {
        printf("Block is not correct!\n");
        test_result = false;
    }

    _free(block_1->contents);
    printf("Test 1 passed!\n");
    return test_result;
}

bool test_2() {
    printf("Starting Test 2...\n");
    bool test_result = true;
    struct block_header* block_1 = block_get_header(_malloc(1024));
    struct block_header* block_2 = block_get_header(_malloc(1024));
    struct block_header* block_3 = block_get_header(_malloc(1024));

    if (!block_1 || !block_2 || !block_3) {
        printf("Failed to create block!\n");
        test_result = false;
    }

    _free(block_2->contents);

    debug_heap(stdout, block_2);

    if (!block_2->is_free) {
        printf("Failed to free block!\n");
        test_result = false;
    }

    _free(block_1->contents);
    _free(block_3->contents);

    printf("Test 2 passed!\n");
    return test_result;
}

bool test_3() {
    printf("Starting Test 3...\n");
    bool test_result = true;
    struct block_header* block_1 = block_get_header(_malloc(1024));
    struct block_header* block_2 = block_get_header(_malloc(1024));
    struct block_header* block_3 = block_get_header(_malloc(1024));
    struct block_header* block_4 = block_get_header(_malloc(1024));

    if (!block_1 || !block_2 || !block_3 || !block_4) {
        printf("Failed to create block!\n");
        test_result = false;
    }

    _free(block_4->contents);
    _free(block_3->contents);

    debug_heap(stdout, block_2);

    if (!block_3->is_free || !block_4->is_free) {
        printf("Failed to free blocks!\n");
        test_result = false;
    }

    if (block_3->next != NULL || block_3->capacity.bytes < 2 * 1024) {
        printf("Failed to merge blocks!\n");
        test_result = false;
    }

    _free(block_2->contents);
    _free(block_1->contents);

    printf("Test 3 passed!\n");
    return test_result;
}

bool test_4() {
    printf("Starting Test 4...\n");
    bool test_result = true;
    struct block_header* block = block_get_header(_malloc(10240));

    if (!block) {
        printf("Failed to create big block!\n");
        test_result = false;
    }

    if (block->capacity.bytes < 10240) {
        printf("Failed to allocate sufficient size!\n");
        test_result = false;
    }

    _free(block->contents);


    if (!block->is_free) {
        printf("Failed to free big block!\n");
        test_result = false;
    }

    printf("Test 4 passed!\n");
    return test_result;
}

bool test_5() {
    printf("Starting Test 5...\n");
    bool test_result = true;
    struct block_header* block_1 = block_get_header(_malloc(10240));
    struct block_header* block_2 = block_get_header(_malloc(10240));
    struct block_header* block_3 = block_get_header(_malloc(1024));


    if (!block_1 || !block_2) {
        printf("Failed to create big blocks!\n");
        test_result = false;
    }

    if (block_1->capacity.bytes < 10240 || block_2->capacity.bytes < 10240) {
        printf("Failed to allocate sufficient size for big blocks!\n");
        test_result = false;
    }

    if (!block_3) {
        printf("Failed to create small block!\n");
        test_result = false;
    }

    _free(block_3->contents);
    _free(block_2->contents);
    _free(block_1->contents);

    if (!block_1->is_free || !block_2->is_free || !block_3->is_free) {
        printf("Failed to free blocks!\n");
        test_result = false;
    }

    printf("Test 5 passed!\n");
    return test_result;
}

void all_tests() {
    heap_init(4096*4);
    if (test_1() && test_2() && test_3() && test_4() && test_5()) printf("All tests passed!\n");
}
